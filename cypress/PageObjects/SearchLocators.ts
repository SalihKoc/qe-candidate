class SearchLocators

{

getSearchBox()
{
    return cy.get('.si-field__placeholder.ng-star-inserted')
}

clickOnFirstResult()
{
    return cy.get('.icon-ground-vehicle')
}

getPreprintId()
{
    return cy.get('h2.si-masthead__b__item.si-subtitle')
}

}

export default SearchLocators;