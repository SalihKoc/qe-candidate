/// <reference types="Cypress" />

import SearchLocators from "../../PageObjects/SearchLocators";

import { Given, When, Then, And } from "cypress-cucumber-preprocessor/steps";

const searchLocators = new SearchLocators();


Given('the user has navigated to the URL {string}', (searchPageUrl) =>
{
    cy.visit(searchPageUrl)
})


When ('the user searches for {string}', (preprintId) =>
{
   searchLocators.getSearchBox().should('be.visible').type(preprintId + '{enter}')  
})


And ('the user clicks on the first result', ()=>
{
    searchLocators.clickOnFirstResult().should('be.visible').click()
})


Then ('the id {string} is present under the title', (preprintId) =>
{
    searchLocators.getPreprintId().should('have.text', preprintId)

    /* @Salih: in case need to print the preprint Id */   
    // searchLocators.getPreprintId().then(function($el) {
    //     cy.log($el.text())
    // })

})